/*Q10 Write a program to print the following pattern
Number of rows = 4
1 2 3 4
2 3 4 5
3 4 5 6
4 5 6 7*/

class Pattern10{

	public static void main(String[] args) {
        int rows = 4;
        int cols = 4;

        for (int i = 0; i < rows; i++) {
            for (int j = 1; j <= cols; j++) {
                System.out.print((i + j) + " ");
            }
            System.out.println(); 
        }
    }
}
