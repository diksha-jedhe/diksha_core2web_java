/*Q5 Write a program to print the following pattern
Number of rows = 3
A C E
G I K
M O Q*/

class Pattern5{
	 public static void main(String[] args) {
        int rows = 3;
        char currentChar = 'A';

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < rows; j++) {
                System.out.print(currentChar + " ");
                currentChar += 2; 
            }
            currentChar = (char) (currentChar - 5);
            System.out.println();
        }
    }
}
