/*Q4 Write a program to print the following pattern
Number of rows = 3
1 1 1
2 2 2
3 3 3*/

class Pattern4{
	public static void main(String[] args) {
        int rows = 3;

        for (int i = 1; i <= rows; i++) {
            for (int j = 0; j < rows; j++) {
                System.out.print(i + " ");
            }
            System.out.println();
        }
    }
}
