/*Q9 Write a program to print the following pattern
Number of rows = 3
C1 C2 C3
C4 C5 C6
C7 C8 C9*/

class Pattern9{

	public static void main(String[] args) {
        int rows = 3;
        int cols = 3;
        int counter = 1;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                System.out.print("C" + counter++ + " ");
            }
            System.out.println(); 
        }
    }
}
