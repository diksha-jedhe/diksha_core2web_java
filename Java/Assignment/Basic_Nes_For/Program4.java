/*Q9 Write a program to print the following pattern
Number of rows = 4
1 2 3 4
4 5 6 7
7 8 9 10
10 11 12 13*/

class Pattern3{

	public static void main(String[] args) {
        int rows = 4;
        int cols = 4;
        int startNumber = 1;

        for (int i = 0; i < rows; i++) {
            int currentNumber = startNumber;
            for (int j = 0; j < cols; j++) {
                System.out.print(currentNumber + " ");
                currentNumber++;
            }
            startNumber += cols;
            System.out.println(); 
        }
    }
}
