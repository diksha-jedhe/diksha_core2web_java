/*Q3 Write a program to print the following pattern
Number of rows = 3
1 2 3
1 2 3
1 2 3*/

class Pattern2{
	public static void main(String[] args) {
        int rows = 3;

        for (int i = 0; i < rows; i++) {
            for (int j = 1; j <= rows; j++) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
    }
}
