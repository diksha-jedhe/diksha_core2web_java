/*Q1 Write a program to print the following pattern.
Number of rows = 3
$# $# $#
$# $# $#
$# $# $#*/


class Pattern1{
	public static void main(String[] args) {
        int rows = 4;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < rows; j++) {
                if (j % 2 == 0) {
                    System.out.print("$");
                } else {
                    System.out.print("#");
                }
                System.out.print("");
            }
            System.out.println();
        }
    }
}
