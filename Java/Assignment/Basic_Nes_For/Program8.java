/*Q8 Write a program to print the following pattern
Number of rows = 4
d c b a
d c b a
d c b a
d c b a*/

class Pattern8{
	public static void main(String[] args) {
        int rows = 4;
        char startChar = 'd';

        for (int i = 0; i < rows; i++) {
            for (char ch = startChar; ch >= 'a'; ch--) {
                System.out.print(ch + " ");
            }
            System.out.println(); 
        }
    }
}
