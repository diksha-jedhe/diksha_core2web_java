/*Q6 Write a program to print the following pattern
Number of rows = 3
1A 1A 1A
1A 1A 1A
1A 1A 1A*/

class Pattern6{
	public static void main(String[] args) {
        int rows = 3;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < rows; j++) {
                System.out.print("1A ");
            }
            System.out.println();
        }
    }
}
