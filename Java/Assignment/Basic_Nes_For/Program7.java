/*Q7 Write a program to print the following pattern
Number of rows = 3
1A 2B 3C
1A 2B 3C
1A 2B 3C*/

class Pattern7{
	public static void main(String[] args) {
        int rows = 3;
        int cols = 3;

        for (int i = 0; i < rows; i++) {
            for (int j = 1; j <= cols; j++) {
                System.out.print(j + "" + (char) (64 + j) + " ");
            }
            System.out.println(); 
        }
    }
}
