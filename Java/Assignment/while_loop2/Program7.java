/*WAP to print the sum of the even digits*/

class Sum{

	public static void main(String[] args){

		int num = 256985;
		int sum = 0;
		int temp = num;

		while(temp > 0){
			int digit = temp % 10;
			if(digit % 2 == 0){
				sum = sum + digit;
                               // System.out.println(sum + " ");
			}
			temp /= 10;
		}
                 System.out.println(sum + " ");
	}
}
