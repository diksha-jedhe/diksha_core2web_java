/*WAP in java using while loop to print the squre of the odd digits of the given number.*/

class Squre{

	public static void main(String[] args) {
        int number = 12345; 

        System.out.print(number + "= ");

        while (number > 0) {
            int digit = number % 10;
            if (digit % 2 != 0) {
                int square = digit * digit;
                System.out.println(square+" ");
            }
            number /= 10;
        }
    }
}

