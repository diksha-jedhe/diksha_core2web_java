/*WAP to prinr the sum of odd digits and product of even digits in a given number.*/

class SumAndProduct{

	public static void main(String[] args) {
        int number = 9367924; 

        int sumOdd = 0;
        int productEven = 1;
        int temp = number;

        while (temp > 0) {
            int digit = temp % 10;
            if (digit % 2 != 0) {
                sumOdd += digit;
            } else {
                productEven *= digit;
            }
            temp /= 10;
        }

        System.out.println("Sum of odd digits in the number " + number + " is: " + sumOdd);
        System.out.println("Product of even digits in the number " + number + " is: " + productEven);
    }
}
