/*WAP in java to print the digits from the given number which is divisible by 2 or 3*/

class DivisibleBy2Or3 {

    public static void main(String[] args) {

        int number = 436780521; 
        
 	       System.out.print("Digits divisible by 2 or 3 in the number " + number + " are: ");
        
	 while (number > 0) {

            int digit = number % 10;

            if (digit % 2 == 0 || digit % 3 == 0) {
                System.out.print(digit + " ");
            }
            number /= 10;
        }
    }
}







