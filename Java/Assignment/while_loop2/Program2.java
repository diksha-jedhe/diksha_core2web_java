/*WAP to print the digits from a given number which is not divisible by 3*/

class NotDivisibleBy3 {
    	public static void main(String[] args) {

        int number = 2569185; 

        System.out.println("Digits not divisible by 3 in the number " + number + " are: ");
        int temp = number;

        while (temp > 0) {
            int digit = temp % 10;
            if (digit % 3 != 0) {
                System.out.println(digit + " ");
            }
            temp /= 10;
        }
    }
}
		





