/*WAP in java using while loopto print the product of digits in a given number*/

class Product{

	public static void main(String[] args) {
        int number = 234; 

        int product = 1;
        int temp = number;

        while (temp > 0) {
            int digit = temp % 10;
            product *= digit;
            temp /= 10;
        }

        System.out.println("Product of digits in the number " + number + " is: " + product);
    }

