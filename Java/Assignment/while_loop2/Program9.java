/*WAP to calculate the sum of odd digits in the number.*/

class SumOfOdd{

	public static void main(String[] args) {
        int number = 2469185; 

        int sum = 0;
        int temp = number;

        while (temp > 0) {
            int digit = temp % 10;
            if (digit % 2 != 0) {
                sum += digit;
            }
            temp /= 10;
        }

        System.out.println("Sum of odd digits in the number " + number + " is: " + sum);
    }
}
