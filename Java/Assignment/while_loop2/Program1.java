/*WAP to print the digits from the given number which is divisible by 2*/

class DivisibleBy2 {

    	public static void main(String[] args) {

        int number = 2569185;
        
        	System.out.println("Digits divisible by 2 are " + " are: ");
        	int temp = number;
        
        while (temp > 0) {
            int digit = temp % 10;
            if (digit % 2 == 0) {
                System.out.println(digit + " ");
            }
            temp /= 10;
        }
    }
}
