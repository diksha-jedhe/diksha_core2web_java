/*WAP in java using while loop to print the cubes of the even digits of a given numbers.*/

class Cube{

	public static void main(String[] args) {

        int number = 216985; 

        int temp = number;

        System.out.print("Cubes of even digits in the number " + number + " in sequential form: ");

        while (temp > 0) {
            int digit = temp % 10;
            if (digit % 2 == 0) {
                int cube = digit * digit * digit;
                System.out.print(cube + " ");
            }
            temp /= 10;
        }
    }
}
