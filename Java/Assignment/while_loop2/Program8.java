/* WAP to print product of odd digits in the given number.*/

class ProductofOdd{

        public static void main(String[] args) {
        int number = 256985;

        int product = 1;
        int temp = number;

        while (temp > 0) {
            int digit = temp % 10;

	    if(digit % 2 != 0){

            	product *= digit;
	    }
            	temp /= 10;
        }

        System.out.println("Product of digits in the number " + number + " is: " + product);
	}
}
