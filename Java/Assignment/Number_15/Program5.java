/* Q9 Write a program to check whether the given number is palindrome or
not.
Input : 12121
Output : 12121 is a palindrome number. */

import java.util.Scanner;
class Palindrome{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the number :");
		int number = sc.nextInt();

		int temp = number;
		int revNum = 0;

		while(number != 0){
			int digit = number % 10;
			revNum = revNum * 10 + digit;
			number /= 10;
		}
		if(temp == revNum){
			System.out.println(temp + " is palindrome number.");
		}else{
			System.out.println(temp + " is not a palindrome number ");
		}
	}
}

