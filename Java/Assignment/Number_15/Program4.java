/* Q7 Write a program to reverse the given number.
Input : 7853
Output : Reverse of 7853 is 3587. */


import java.util.Scanner;

class ReverseNumber {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter a number: ");
        int number = sc.nextInt();
      	int revNum = 0;
        while (number != 0) {
            int digit = number % 10;
            revNum = revNum * 10 + digit;
            number /= 10;
        }

        System.out.println("Reverse of " + number + " is " + revNum + ".");
    }
}

