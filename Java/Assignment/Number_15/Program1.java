/* Write a program to print the factors of a given number.
Input: 12
Output: Factors of 12 are 1,2,3,4,6,12 */

import java.util.*;
class Factorial{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		int fact = 1;

		System.out.print("Enter the Number :");
		int num = sc.nextInt();

		while(num >= 1){
			fact = fact * num;
			num--;
		}
		System.out.println("the factorial of is "  +  fact) ;
	}
}
