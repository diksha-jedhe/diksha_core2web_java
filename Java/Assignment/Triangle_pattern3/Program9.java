/* 9. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4


19 17 15 13
11 9 7
5 3
1   */

import java.util.Scanner;

class Pattern9 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();
        int start = 19;

       for(int i = 1;i <= rows; i++){
       		for(int j = 1;j <= rows-i+1; j++){
			System.out.print(start +" ");
			start -= 2;
		}
		System.out.println();
       }
    }
}

