/*2. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4
2 4 6 8
10 12 14
16 18
20 */

import java.util.Scanner;
class Pttern2{
	public static void main(String[] args){

		Scanner scanner = new Scanner(System.in);

        	System.out.print("Enter the number of rows: ");
        	int rows = scanner.nextInt();

        	int num = 2;
        	for (int i = 1; i <= rows; i++) {
            		for (int j = 1; j <= rows - i + 1; j++) {
                		System.out.print(num + " ");
                		num += 2;
            		}
            		System.out.println();
        	}
    }
}
