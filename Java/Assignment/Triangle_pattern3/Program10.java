/* *10. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3

c b a
B A
a  */


import java.util.Scanner;

class Pattern10 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the number of rows: ");

        int rows = scanner.nextInt();
        int startAscii;

        for (int i = rows; i >= 1; i--) {
            if (rows % 2 == 0) {
                startAscii = 65 + i - 1; // ASCII value of 'A'
            } else {
                startAscii = 97 + i - 1; // ASCII value of 'a'
            }
            for (int j = 0; j < i; j++) {
                System.out.print((char) (startAscii - j) + " ");
            }
            System.out.println();
        }
    }
}

	

		
