/* 3. WAP in notebook & Dry run first then type
Take number of rows from user :

Rows = 4
20 18 16 14
12 10 8
6 4
2  */


import java.util.Scanner;

class Pattern3 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();

        int num = rows * 2;
        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= rows - i + 1; j++) {
                System.out.print(num + " ");
                num -= 2;
            }
            System.out.println();
            num = (rows - i) * 2;
        }
    
    }
}
