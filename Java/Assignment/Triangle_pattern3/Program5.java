/* 5. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
A B C
a b
A  */

import java.util.Scanner;

class Pattern5 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();

        char upperChar = 'A';
        char lowerChar = 'a';

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < rows - i; j++) {
                if (i % 2 == 0) {
                    System.out.print(upperChar + " ");
                    upperChar++;
                } else {
                    System.out.print(lowerChar + " ");
                    lowerChar++;
                }
            }
            System.out.println();
        }
    }
}

