/*  7. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4
4 c 2 a
3 b 1
2 a
1   */


import java.util.Scanner;

class Pattern7{

	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter the number of rows :");
		int rows = scanner.nextInt();

		for(int i = 0; i >= 1; i--){
			for (int j = 1; j <= i; j++) {
                		System.out.print((rows - j + 1) + " ");
            		}
            			for (int k = 1; k <= rows - i; k++) {
                			System.out.print((char) ('a' + k - 1) + " ");
            			}
            			System.out.println();
        	}
    	}
}
