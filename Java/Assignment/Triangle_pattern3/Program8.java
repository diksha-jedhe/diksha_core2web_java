/* 8. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4

4 3 2 1
C B A
2 1
A
*/

import java.util.Scanner;

class Pattern8{

	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enetr the number of rows :");
		int rows = scanner.nextInt();
		int currChar = 64;

		for(int i = rows; i >= 1;i--){
			for(int j = rows; j >= i; j--){
				System.out.print(j + " ");
			}
			if(i % 2 == 0){
				for(int k = 1; k <= rows-i+1;k++){
					System.out.print((char)(currChar + k)+" ");
				}
			}else{
				for(int k = 1; k <= rows-i+1;k++){
					System.out.print((char)(currChar-k+i)+" ");
				}
			}
			System.out.println();
		}
	}
}

