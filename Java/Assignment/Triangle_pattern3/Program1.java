/*1. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4
1 2 3 4
2 3 4
3 4
4 */


import java.util.Scanner;
class Pattern1{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the number of rows :");
		int rows = sc.nextInt();

		for (int i = 1; i <= rows; i++) {
            		for (int j = i; j <= rows; j++) {
               			 System.out.print(j + " ");
            		}
            		System.out.println();
        	}
	}
}

