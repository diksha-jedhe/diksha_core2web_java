/*
6. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
1 a 2
1 a
1   */


import java.util.Scanner;

class Pattern6 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();

        for (int i = 0; i < rows; i++) {
            int num = 1;
            char character = 'a';

            for (int j = rows - i; j > 0; j--) {
                if (j % 2 == 0) {
                    System.out.print(num + " ");
                    num++;
                } else {
                    System.out.print(character + " ");
                }
                if (j > 1 && (j - 1) % 2 == 0) {
                    System.out.print(character + " ");
                    character++;
                }
            }
            System.out.println();
        }
    }
}

