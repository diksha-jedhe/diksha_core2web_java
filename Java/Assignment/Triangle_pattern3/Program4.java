/* 4. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
F E D
C B
A */


import java.util.Scanner;

class Pattern4 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();

        char currChar = 'A';

        for (int i = 0; i < rows; i++) {
            for (int j = rows - i; j > 0; j--) {

                System.out.print(currChar + " ");
		currChar++;
            }
            System.out.println();
        }
    }
}
