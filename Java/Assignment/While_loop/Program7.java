/*7. Write a program to count the digits in the given number.
Input: num = 93079224
Output: Count of digits= 8*/

class Count{
	public static void main(String[] args) {
        	long num = 93079224;
        	int count = 0;

  
        	while (num != 0) {
            		num = num / 10; 
            		count++; 
        	}

        	System.out.println("Count of digits = " + count);
    	}
}
