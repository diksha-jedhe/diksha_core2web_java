/*5. Row= 4
1 3 5 7
9 11 13 15
17 19 21 23
25 27 29 31*/

class Pattern{
	public static void main(String[] args) {
        int rows = 4;
        int Number = 1;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < rows; j++) {
                System.out.print(Number + " ");
                Number += 2; 
            }
            System.out.println(); 
        }
    }
}
