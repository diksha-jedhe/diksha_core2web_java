/*6. Write a program to print each digit on a new line of a given number
using a while loop
Input: num = 9307
Output: 7
0
3
9*/

class Digits{
	  public static void main(String[] args) {
          int num = 9307;

           while (num > 0) {
            	int digit = num % 10; 
            	System.out.println(digit);
            	num = num / 10; 
	   }
        
    }
}
