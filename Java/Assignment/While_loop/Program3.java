/*3. Write a program to print the character sequence given below when the
range given is
Input : start = 1 and end =6.
This means iterate the loop from 1 to 6
Output: A B C D E F*/

class ABCD{

	public static void main(String[] args){
		 int start = 1;
        	int end = 6;

      
        for (int i = start; i <= end; i++) {
            char ch = (char) ('A' + i - 1);
            System.out.print(ch + " ");
        }
	System.out.println();
    }
}
