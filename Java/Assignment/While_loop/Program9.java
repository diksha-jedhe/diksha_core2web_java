/*9. Write a program to count the odd digits and even digits in the given
number.
Input: 214367689
Output: Odd count : 4
Even count : 5*/


class DigitCounter {
    public static void main(String[] args) {
        long num = 214367689;
        int oddCount = 0;
        int evenCount = 0;

        while (num > 0) {
            int digit = (int)(num % 10); 
            if (digit % 2 != 0) { 
                oddCount++; 
            } else {
                evenCount++; 
            }
            num = num / 10; 
        }

        System.out.println("Odd count: " + oddCount);
        System.out.println("Even count: " + evenCount);
    }
}
