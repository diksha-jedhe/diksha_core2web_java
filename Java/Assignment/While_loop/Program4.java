/*4. Write a program to print the character sequence given below where the
input: start=1, end =6
This means iterate the loop from 1 to 6
Output: A 2 C 4 E 6 (if num is odd print the character)*/

class Demo{
	public static void main(String[] args) {
        int start = 1;
        int end = 6;

        for (int i = start; i <= end; i++) {
            if (i % 2 == 0) {
                System.out.print(i + " "); 
            } else {
                char correspondingChar = (char) ('A' + i - 1); 
                System.out.print(correspondingChar + " "); 
            }
        }
        System.out.println(); 

    }
}
