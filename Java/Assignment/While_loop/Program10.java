/*10.Write a program to print the sum of digits in the given number.
Input: 9307922405
Output: sum of digits in 9307922405 is 41*/


class DigitSum{

    public static void main(String[] args) {

        long num = 9307922405l;
        long originalNum = num;
        long sum = 0;

        while (num > 0) {
            long digit = num % 10; 
            sum += digit; 
            num = num / 10; 
        }

        System.out.println("Sum of digits in " + originalNum + " is " + sum + ".");
    }
}
