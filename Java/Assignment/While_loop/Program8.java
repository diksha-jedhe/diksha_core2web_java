/*8. Write a program to print the odd digits of a given number.
Input : 216985
Output : 5 9 1*/

class OddDigit{

    	public static void main(String[] args) {
        	long num = 216985;

        
        	while (num > 0) {
            		int digit = (int)(num % 10);
            		if (digit % 2 != 0) { 
    				System.out.print(digit + " ");       
                         }
            		num = num / 10; 
        	}
    		System.out.println();       
    	}
}
