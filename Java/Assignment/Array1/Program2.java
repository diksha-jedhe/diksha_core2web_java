/* 2. Take an input from the user where the size of the array should be 10 and print the
output of the user given elements of an array. */


import java.util.Scanner;
class  PrintArray{

	public static void main(String[] args){
        	Scanner scanner = new Scanner(System.in);

        	int[] array = new int[10];

        	System.out.println("Enter 10 elements:");

        	for (int i = 0; i < array.length; i++) {
            		System.out.print("Enter element " + (i + 1) + ": ");
            		array[i] = scanner.nextInt();
        	}

        	System.out.println("Array:");
        	for (int i = 0; i < array.length; i++) {
            		System.out.print(array[i] + " ");
        	}
	}
}
    

