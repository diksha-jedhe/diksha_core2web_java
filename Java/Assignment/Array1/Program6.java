/* 6. Write a program where you have to take input from the user for a character array and
print the characters. */


import java.util.Scanner;
class CharArray{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter The Size of Character array:");
		int size = sc.nextInt();

		char[] charArray = new char[size];

		System.out.println("Enter "+ size + "characters :");

		for(int i = 0;i < size; i++){
			System.out.print("Enter character " + (i + 1) + ": ");
			charArray[i] = sc.nextInt().charAt(0);
		}
		 System.out.println("Characters in the array:");

        	 for (int i = 0; i < size; i++) {
            		System.out.print(charArray[i] + " ");
        	}
	}
}
