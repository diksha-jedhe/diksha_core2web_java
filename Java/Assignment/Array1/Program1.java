/* 1. Write a program to print the array with minimum 10 elements data.
Example:
Array:
10 20 30 40 50 60 70 80 90 100 */

class PrintArray{

	public static void main(String[] args){

		int[] array = {10,20,30,40,50,60,70,80,90,100};

		System.out.println("Array :");
		for(int i = 0;i < array.length; i++){

			System.out.println(array[i] + " ");
		}
	}
}
