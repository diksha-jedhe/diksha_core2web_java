/* 8. Write a program where you have to store the employee’s age working at a company,
take count of employees from the user.Take input from the user. */


import java.util.Scanner;

class EmployeeAges {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter the number of employees: ");
        int count = sc.nextInt();

        int[] ages = new int[count];

        System.out.println("Enter the ages of " + count + " employees:");

        for (int i = 0; i < count; i++) {
            System.out.print("Enter age for employee " + (i + 1) + ": ");
            ages[i] = sc.nextInt();
        }

        System.out.println("Employee ages stored successfully.");

    }
}

