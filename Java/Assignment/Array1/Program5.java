/* 5. Write a program where you have to print the elements from the array which are less
than 10.Take input from the user.
Example:
Enter size: 10

11 2 18 9 10 5 16 20 8 10
Output :
2 is less than 10
9 is less than 10
5 is less than 10
8 is less than 10 */

import java.util.Scanner;
class Elements{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the Size :");
		int size  = sc.nextInt();

                int[] array = new int[size];
		System.out.println("Enter " + size + "Elements :");

		for(int i = 0;i < size; i++){
			System.out.print("Enter element " + (i + 1)+ ": ");
			array[i] = sc.nextInt();
		}
		for(int i = 0;i < size; i++){
			if(array[i] < 10){

				System.out.println(array[i] + " is less than 10");
			}
		}
	}
}
		

