/* 7. Write a program to print the elements of the array which is divisible by 4. Take input
from the user.
Example:
Enter size: 10
14 20 18 9 11 51 16 2 8 10
Output:
20 is divisible by 4 and its index is 1
16 is divisible by 4 and its index is 6
8 is divisible by 4 and its index is 8 */

import java.util.Scanner;
class Divisible{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size :");
		int size = sc.nextInt();

		int[] array = new int[size];
		
		System.out.println("Enter " + size + " Element :");

		for(int i = 0; i < size; i++){
			System.out.print("Enter element " + size + " Element :");
			System.out.print("Enter element " + (i + 1) + ": ");
            		array[i] = sc.nextInt();
        	}

        	System.out.println("Output:");

        	for (int i = 0; i < size; i++) {
            		if (array[i] % 4 == 0) {
                		System.out.println(array[i] + " is divisible by 4 and its index is " + i);
            		}
        	}
	}
}
