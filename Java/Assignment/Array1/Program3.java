/* 3. Write a program to print the even elements in the array. Take input from the user.
Example :
Enter size : 10
Array:
10 11 12 13 14 15 16 17 18 19 */


import java.util.Scanner;

class Even{
	public static void main(String[] args){
 
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter The Size :");
		int size = sc.nextInt();

		int[] array = new int[size];

		System.out.println("Enter " + size + " elements:");

		for(int i = 0;i < size; i++){
			System.out.println("Enter element " + (i + 1) + ": ");
			array[i] = sc.nextInt();
		}
		System.out.println("Even elements in the array:");
        	for (int i = 0; i < size; i++) {
            		if (array[i] % 2 == 0) {
                		System.out.print(array[i] + " ");
            		}
        	}
	}
}
