/*  1. WAP in notebook & Dry run first then type
Take a number of rows from the user :
row=3
C d e
F g h
I j k  */


import java.util.Scanner;

class Pattern1{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of rows :");
		int rows = sc.nextInt();

		char startChar = 'C';
		for(int i = 0;i < rows; i++){
			//System.out.print(currentChar + " ");
			char currentChar = startChar;

			for(int j = 0;j < 2; j++){
				//currentChar++;
				System.out.print(currentChar + " ");
				currentChar++;

			}
			System.out.println();
			startChar += 3;
		}
	}
}


