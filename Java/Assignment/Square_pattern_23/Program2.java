/* 2. WAP in notebook & Dry run first then type
Take the number of rows from the user :
row=3
c d E
f G H
I J K
row=4
d e f G
h i J K
l M N O
P Q R S */

import java.util.Scanner;

class Pattern2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of rows :");
		int rows = sc.nextInt();

		char startChar = 'c';
		for(int i = 0;i < rows;i++){
			char currentChar = startChar;
			for(int j = 0;j <= i;j++){
			System.out.print(currentChar +" ");
			currentChar++;
		}
		System.out.println();
		startChar += i + 2;
		}
		
	}
}
