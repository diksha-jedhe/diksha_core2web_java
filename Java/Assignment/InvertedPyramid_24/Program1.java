/*
Rows = 3
1       1       1       1       1
        1       1       1
                1
Rows = 4
1       1       1       1       1       1       1
        1       1       1       1       1
                1       1       1
                        1
*/

import java.util.*;

class Pattern1{

	public static void main(String [] args){
	
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Rows = ");
		int row = sc.nextInt();

		for(int i=1; i<=row; i++){
		
			for(int j=1; j<i; j++){
			
				System.out.print("\t");
			}

			for(int j=1; j<=(row-i)*2+1; j++){
			
				System.out.print("1\t");
			}
			System.out.println();
		}
	}
}

