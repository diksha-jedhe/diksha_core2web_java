/*
Rows = 4
1       2       3       4       5       6       7
        1       2       3       4       5
                1       2       3
                        1
*/

import java.util.*;

class Pattern4{

	public static void main(String [] args){
	
		Scanner sc = new Scanner(System.in);
	
		System.out.print("Rows = ");
		int row = sc.nextInt();

		for(int i=1; i<=row; i++){
			int num=1;		
			for(int s=1; s<i; s++){
			
				System.out.print("\t");
			}

			for(int j=1; j<=(row-i)*2+1; j++){
			
				System.out.print(num+++"\t");
			}
			System.out.println();
		}
	}
}

