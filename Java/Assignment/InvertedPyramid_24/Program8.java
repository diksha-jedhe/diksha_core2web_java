/*
Rows = 4
1       2       3       4       3       2       1
        2       3       4       3       2
                3       4       3
                        4
*/
import java.util.*;

class Pattern8{

	public static void main(String [] args){
	
		Scanner sc = new Scanner(System.in);
	
		System.out.print("Rows = ");
		int row = sc.nextInt();

		int num = 1;
		
		for(int i=1;i<=row;i++){
		
			for(int s=1;s<i;s++){
			
				System.out.print("\t");
			}

			for(int j=1;j<=(row-i)*2+1;j++){
			
				if(j<(row-i+1)){
				
					System.out.print(num+++"\t");
				}else{
					System.out.print(num--+"\t");
				}
			}
			num+=2;;
			System.out.println();
		}
	}
}

