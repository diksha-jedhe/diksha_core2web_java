/*
Rows = 4
A       B       C       D       C       B       A
        A       B       C       B       A
                A       B       A
                        A
*/

import java.util.*;

class Pattern7{

	public static void main(String [] args){
	
		Scanner sc = new Scanner(System.in);
	
		System.out.print("Rows = ");
		int row = sc.nextInt();

		for(int i=1; i<=row; i++){
		
			char ch = 'A';
			for(int s=1; s<i; s++){
			
				System.out.print("\t");
			}

			for(int j=1; j<=(row-i)*2+1; j++){
			
				if(j<((row-i)+1)){
				
					System.out.print(ch+++"\t");
				}else{
					System.out.print(ch--+"\t");
				}
			}
			System.out.println();
		}
	}
}

