/*
 Rows = 5
E D C B A
  E D C B
    E D C
      E D
        E
*/

import java.util.*;

class Pattern9 {

        public static void main(String[] args) {

                Scanner sc = new Scanner(System.in);

                System.out.print("Rows = ");
                int rows = sc.nextInt();  

		for(int i=rows; i>=1; i--){

			int ch = 64 + rows;
			for(int j=i; j<rows; j++)
				System.out.print("  ");
			for(int j=1; j<=i; j++)
				System.out.print((char)ch-- + " ");
			System.out.println();
		}
	}                                                                                                                            } 
}
