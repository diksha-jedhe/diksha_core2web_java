/*
 Rows = 4
      D
    C D
  B C D
A B C D
*/

import java.util.*;

class Pattern3 {

        public static void main(String[] args) {

                Scanner sc = new Scanner(System.in);

                System.out.print("Rows = ");
                int rows = sc.nextInt();    
		
	       	for(int i=rows; i>=1; i--){
		
			int ch = 64 + i;

			for(int j=i-1; j>=1; j--)
				System.out.print("  ");
			for(int j=i; j<=rows; j++)
				System.out.print((char)ch++ + " ");
			System.out.println();
		}	

	}
}	
