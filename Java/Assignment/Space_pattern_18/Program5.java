/*
 Rows = 4
      1
    2 4
  3 6 9
4 8 12 16
*/

import java.util.*;

class Pattern5 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Rows = ");
		int rows = sc.nextInt();

		for(int i=1; i<=rows; i++){

			for(int j=rows-1; j>=i; j--)
				System.out.print("  ");
			for(int j=1; j<=i; j++)
				System.out.print(i*j + " ");
			System.out.println();
		}
	}
}

