/*
Rows = 4
1 2 3 4
  1 2 3
    1 2
      1
*/

import java.util.*;

class Pattern7 {

        public static void main(String[] args) {

                Scanner sc = new Scanner(System.in);

                System.out.print("Rows = ");
                int rows = sc.nextInt();    
		
		for(int i=rows; i>=1; i--){

			for(int j=i; j<rows; j++)
				System.out.print("  ");
			for(int j=1; j<=i; j++)
				System.out.print(j + " ");
			System.out.println();
		}
	}
} 

