/*
 Rows = 4
65 B  67 D
   B  67 D
      67 D
         D
*/

import java.util.*;

class Pattern {

        public static void main(String[] args) {

                Scanner sc = new Scanner(System.in);

                System.out.print("Rows = ");
                int rows = sc.nextInt();

		for(int i=1; i<=rows; i++){
		
			int ch = 64 + i;
			for(int j=2; j<=i; j++)
				System.out.print("   ");
			for(int j=i; j<=rows; j++){
				if(j%2==0)
					System.out.print((char)ch++ + "  ");
				else
					System.out.print(ch++ + " ");
			}
			System.out.println();
		}
	}                                                                                                                            } 
}
