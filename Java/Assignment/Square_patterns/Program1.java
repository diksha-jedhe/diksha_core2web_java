/*  1. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3

C B A
3 3 3
C B A     */


import java.util.Scanner;

class Pattern1{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the No of rows :");
		int rows = sc.nextInt();

		for(int i = 1; i <= 3; i++){
			for(int j = rows ; j >= 1; j--){
				if(i == 2){
					System.out.print(rows + " ");
				}else{
					System.out.print((char)('A' + j - 1) + " ");
				}
			}
			System.out.println();
		}
	}
}


