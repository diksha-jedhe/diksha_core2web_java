/* 3. WAP in notebook & Dry run first then type
Take number of rows from user :
row =3
9 4 5
36 7 8
81 10 11  */  


import java.util.*;
class Pattern3{

	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Number of Rows :");
		int rows = sc.nextInt();

		int temp = rows;
		for(int i = 1;i <= 3; i++){
			for(int j = 1;j <= rows; j++){
				if(j == 1){
					System.out.print(temp * temp + " ");
				}else{
					System.out.print(temp + " ");
				}
				temp++;
			}
			System.out.println();
			
		}
	}
}

