import java.util.*;
class Pattern7{

	public static void main(String[]args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the no of rows :");
		int rows = sc.nextInt();
		int temp = rows;
		char ch = 'A';

		for(int i = 1 ; i <= rows; i++){
			for(int j = 1 ; j <= rows; j++){
				if(temp % 2 == 1){
					System.out.print(ch + " ");
				}
				else{
					System.out.print(temp +" ");
				}
				temp ++;
			}
			//temp ++;
			ch++;
			System.out.println();
		}
	}
}


