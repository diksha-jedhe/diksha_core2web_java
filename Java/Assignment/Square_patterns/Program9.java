/* 9. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
2 6 6
3 4 9
2 6 6
row=4
2 6 6 12
3 4 9 8
2 6 6 12
3 4 9 8 */

import java.util.*;
class Pattern9{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter No of Rows :");
		int rows = sc.nextInt();
		int num = 1;

		for(int i = 1; i <= rows; i++){
			for(int j = 1; j <= rows; j++){
				if((i+j) % 2 == 1){
					System.out.print(num*3 + " ");
				}else{
					System.out.print(num*2 + " ");
				}
			}
			System.out.println();
		}
	}
}


