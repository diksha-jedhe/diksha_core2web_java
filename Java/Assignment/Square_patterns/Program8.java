/* 8. WAP in notebook & Dry run first then type
Take number of rows from user :

row=3
# C #
C # B
# C #
row=4
# D # C
D # C #
# D # C
D # C # */

import java.util.*;
class Pattern8{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number of rows :");
		int row = sc.nextInt();
		int temp = row;
		char ch = 'C';

		for(int i = 1;i <= row; i++){
			for(int j = 1;j <= row; j++){
				if(temp % 2 == 1){
					System.out.print("#" + " ");
				}
				else{
					System.out.print(ch-- + " ");
				}
				temp++;
			}
			System.out.println();
		}
	}
}
					
