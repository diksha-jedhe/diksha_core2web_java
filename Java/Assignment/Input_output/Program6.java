/*Write a program to take a range as a input from the user and print the numbers between them*/

import java.util.Scanner;

class Range{

	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter the starting number of the range :");
		int start = scanner.nextInt();

		System.out.println("Enter the Ending number of the range :");
		int end = scanner.nextInt();

		System.out.println("Numbers between " + start + " and " + end +":");

		for(int i = start+1; i < end; i++){
			System.out.print(i + " ");
		}
		System.out.println();

	}
}
