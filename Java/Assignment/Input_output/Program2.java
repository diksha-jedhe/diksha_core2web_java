/*2.Write a program to take the age of the voter from the user where the age should be positive and check whether he or she is eligible for voting.(means negantive values r not allowed).*/

class Voter{

	public static void main(String[] args) {

        	Scanner scanner = new Scanner(System.in);

        	System.out.print("Enter your age: ");
        	int age = scanner.nextInt();

        	if (age >= 0) {
            		if (age >= 18) {
                		System.out.println("You are eligible to vote.");
            	} else {
                		System.out.println("You are not eligible to vote. You must be at least 18 years old.");
            	}
        	} else {
           			System.out.println("Invalid age entered. Age must be a positive number.");
        	}
    }
}
