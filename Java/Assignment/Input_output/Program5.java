/*Write a program to take user and check whether the number is present in the table of 16 or not*/


import java.util.Scanner;

class NumTable{
	public static void main(String[] args){

		Scanner scanner = new Scanner(System.in);

		System.out.print("Enter the number :");
		int num = scanner.nextInt();

		boolean found = false;
		int multiple = 16;
		int i = 1;

		while(i <= 10){
			if(num == multiple * i){
				found = true;
				break;
			}
			i++;
		}
		if(found){
			System.out.println(num +" is present in the table of 16.");
		}else{
			System.out.println(num +" is not present in the table of 16.");
		}
	}
}

	
