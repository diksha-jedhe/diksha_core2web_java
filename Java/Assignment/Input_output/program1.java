/*Write a program to take a marks of 3 subjects from the user and print the total obtaine marks out of total(100 marks each subject) . input 1:math = 95,english = 96,science = 99.marathi = 97 output : 387 out of 400*/

import java.util.Scanner;

class Marks{

	public static void main(String[] args){

		Scanner scanner = new Scanner(System.in);

		System.out.println("Maths :");
		int mathMarks = scanner.nextInt();

		System.out.println("English :");
		int englishMarks = scanner.nextInt();

		System.out.println("Science :");
		int scienceMarks = scanner.nextInt();
		
		System.out.println("Marathi :");
		int marathiMarks = scanner.nextInt();

		int TotalMarks = mathMarks + englishMarks + scienceMarks + marathiMarks;
		int TotalmainMarks = 100 * 4;

		System.out.println("Total obtained marks ==> " + TotalMarks  + " out of " + TotalmainMarks);
	}
}
