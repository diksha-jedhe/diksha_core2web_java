/*Write a program to take a number as a input from the user and print the reverse table of it*/

import java.util.Scanner;

class ReverseTable{

        public static void main(String[] args){

		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter the number for print its reverse table :");
		int num = scanner.nextInt();
		System.out.println("Reverse table of " + num + ":");

		for(int i = 10;i >= 1; i--){
			System.out.println(num +" x "+ i + "=" + (num * i));

		}
	}
}
