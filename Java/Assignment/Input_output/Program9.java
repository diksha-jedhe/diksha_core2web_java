/*Write a program to take the input from the user and print the even numbers*/


import java.util.Scanner;

class EvenNum{
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter the limits :");
		int limit = scanner.nextInt();

		System.out.println("Even number" + limit + ":");

		for(int i =1;i <= limit; i++){
			if(i % 2 == 0){
				System.out.print(i + " ");
			}
		}
		System.out.println();
	}
}
