/*Write a program to take range from user and print the sum of the numbers in the range*/


import java.util.Scanner;

class Sum{
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter the starting number of the range");
		int start = scanner.nextInt();

		System.out.println("Enter the Ending number of the range");
		int end = scanner.nextInt();

		int sum = 0;
		for(int i = start; i<= end; i++ ){
			sum = sum+i;
		}
		System.out.println("Sum of numbers between " + start + "and" + end+ "is :" + sum);

	}
}
