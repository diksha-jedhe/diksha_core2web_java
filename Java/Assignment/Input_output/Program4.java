/*Write a program to take a number as input from the user and print its Table*/


import java.util.Scanner;

class Table{

	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter your number :");
		int num = scanner.nextInt();
		System.out.println("Table of"+ num +":");

		for(int i = 1; i <= 10; i++){
			System.out.println(num + " x " + i + "=" + (num * i));
		}
	}
}
