/*Write a program to take a number from the user and check whether the number is divisible by 8 or not*/

import java.util.Scanner;

class Divisible{

	public static void main(String[] args){

		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter the number :");
		int num = scanner.nextInt();

		if(num % 8 == 0){
			System.out.println(num + " is divisible by 8. ");
		}else{
			System.out.println(num + " is not  divisible by 8. ");
		}
	}
}

		
    		


