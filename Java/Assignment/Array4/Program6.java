import java.io.*;
class Array6{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter size : ");
                int size=Integer.parseInt(br.readLine());
                System.out.println("Enter array elements : ");
                char arr[]=new char[size];
                for(int i=0;i<size;i++){
                        arr[i]=(char)br.read();
                }

                int countVowels=0;
                int countCon=0;

                for(int i=0;i<size;i++){
                        if(arr[i]!='a' && arr[i]!='e' && arr[i]!='i' && arr[i]!='o' && arr[i]!='u'){
                               countCon++;
                        }
                        else{
                                countVowels++;
                        }
                }

                        System.out.println("The count of vowels in arrays is:  " +countVowels);
                        System.out.println("The count of consonants in arrays is:  " +countCon);


        }
}
