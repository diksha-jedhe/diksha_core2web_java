/*Array4 program1*/

import java.util.*;
class Array1{
        public static void main(String[] args){
                Scanner sc=new Scanner(System.in);
                System.out.print("Enter size : ");
                int size=sc.nextInt();
                System.out.println("Enter array elements : ");
                int arr[]=new int[size];
                for(int i=0;i<size;i++){
                        arr[i]=sc.nextInt();
                }
                int avg=0; int sum=0;
                for(int i=0;i<size;i++){
                        sum+=arr[i];
                }
                avg=sum/size;
                System.out.println("Array element's average is : " +avg );
        }
}
