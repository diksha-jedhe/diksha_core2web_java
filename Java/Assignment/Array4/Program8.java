import java.io.*;
class Array8{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter size : ");
                int size=Integer.parseInt(br.readLine());
                System.out.println("Enter array elements : ");
                char arr[]=new char[size];
                for(int i=0;i<size;i++){
                        arr[i]=(char)br.read();
                }
                System.out.print("Enter character to check :" );
                char check=(char)br.read();

                int count=0;
                for(int i=0;i<size;i++){
                        if(arr[i]==check){
                                count++;
                              }

                }
                        System.out.println( check + " occurse " + count + " times in the given array .");


        }
}
