/*
Rows = 3
    A
  a b a
A B C B A

Rows = 3
    A
  a b a
A B C B A
*/

import java.util.*;

class Pattern9 {

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Rows = ");
                int rows = sc.nextInt();

                for(int i=1; i<=rows; i++){

			int ch1 = 65;
			int ch2 = 97;
                        for(int j=rows-1; j>=i; j--)
				System.out.print("  ");
			for(int j=1; j<=i; j++){
				if(i%2==1)
					System.out.print((char)ch1++ + " ");
				else
					System.out.print((char)ch2++ + " ");
			}
			ch1--;
			ch2--;
			for(int j=1; j<=i-1; j++){
				if(i%2==1)
					System.out.print((char)--ch1 + " ");
				else
					System.out.print((char)--ch2 +" ");
			}
			System.out.println();     
		} 
	}
}  

