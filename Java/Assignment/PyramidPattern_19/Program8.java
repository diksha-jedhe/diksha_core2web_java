/*
Rows = 3
    3
  2 2 2
1 1 1 1 1

Rows = 4
      4
    3 3 3
  2 2 2 2 2
1 1 1 1 1 1 1
*/

import java.util.*;

class Pattern8 {

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Rows = ");
                int rows = sc.nextInt();

                for(int i=rows; i>=1; i--){

                        for(int j=i-1; j>=1; j--){
				System.out.print("  ");

			for( j=rows; j>=i; j--)
				System.out.print(i + " ");

			for(j=i; j<=rows-1; j++)
				System.out.print(i + " ");
			System.out.println();                                                                                           
			}                                                                                                              
	       	}                                                                                                               
	}  
}

