/*
 Rows = 3
    A
  B B B
C C C C C

Rows = 4
      A
    B B B
  C C C C C
D D D D D D D
*/
 

import java.util.*;

class Pattern4 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Rows = ");
		int rows = sc.nextInt();

		char ch = 'A';
		for(int i = 1; i<=rows; i++){

			for(int j=rows-1; j>=i; j--){

				System.out.print("  ");
			}
			for(int j=1; j<=i; j++){

				System.out.print(ch + " ");
			}
			for(int j=2; j<=i; j++){

				System.out.print(ch + " ");
			}
			System.out.println();
			ch++;
		}
	}
}

