/*
Rows = 3
    3
  3 2 3
3 2 1 2 3

Rows = 4
      4
    4 3 4
  4 3 2 3 4
4 3 2 1 2 3 4
*/

import java.util.*;

class Pattern6 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Rows = ");
		int rows = sc.nextInt();

		for(int i=rows; i>=1; i--){

			for(int j=1; j<=i-1; j++)
				System.out.print("  ");
			for(int j=rows; j>=i; j--)
				System.out.print(j + " ");
			for(int j=i+1; j<=rows; j++)
				System.out.print(j + " ");
			System.out.println();
		}
	}
}

