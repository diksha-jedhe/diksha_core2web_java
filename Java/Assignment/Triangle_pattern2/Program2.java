/* 2.Write a program to print the given pattern
rows=3
a
$ $
a b c  */

import java.util.Scanner;
class Pattern2{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the number of rows :");
		int rows = sc.nextInt();

		char ch = 'a';

		for(int i = 1; i <= rows; i++){
			if(i == 1){
				System.out.println(ch);
			}else if(i == 2){
				System.out.println("$ $");
			}else{
				for(int j = 0; j < i; j++){
					System.out.print(ch + " ");
					ch++;
				}
				ch = 'a';
				System.out.println();
			}
		}
	}
}
