/* 4.Write a program to print the given pattern
rows=3
c
C B
c b a   */

import java.util.Scanner;

class Pattern4 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();

        for (int i = 0; i < rows; i++) {
            char ch;
            if (i % 2 == 0) {
                ch = 'c';
            } else {
                ch = 'C';
            }
            for (int j = 0; j <= i; j++) {
                System.out.print(ch);
                if (ch != 'a' && ch != 'A') {
                    System.out.print(" ");
                }
                ch--;
            }
            System.out.println();
        }

       
    }
}

