/* 3.Write a program to print the given pattern
rows=3
C
C B
C B A */

import java.util.Scanner;

class Pattern3 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();

        for (int i = 0; i < rows; i++) {
            char ch = 'C';
            for (int j = 0; j <= i; j++) {

                System.out.print(ch + " ");
                ch--;
            }
            System.out.println();
        }

    }
}

