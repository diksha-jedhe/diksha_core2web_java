/* 6. Write a program to print the given pattern
rows=3
1
B C
1 2 3  */

import java.util.Scanner;

class Pattern6 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the number of rows: ");

        int rows = scanner.nextInt();

        char ch = 'A';
        int num = 1;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j <= i; j++) {

                if (i % 2 == 0) {
                    System.out.print(num + " ");
                    num++;
                } else {
                    System.out.print(ch + " ");
                    ch++;
                }
            }
            System.out.println();
        }
    }
}

