/* 9. Write a program to print the given pattern
rows =3

4
4 a
4 b 6 */


import java.util.Scanner;

class Pattern9 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();

        int num = rows + 3; 

        for (int i = 0; i < rows; i++) {
            char ch = (char) ('a' + i);
            for (int j = 0; j <= i; j++) {
                if (j == i) {
                    System.out.print(ch + " ");
                    ch++;
                } else {
                    System.out.print(num + " ");
                    num++;
                }
            }
            System.out.println();
        }

    }
}

