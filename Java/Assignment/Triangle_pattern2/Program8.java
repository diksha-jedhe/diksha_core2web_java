/* 8. Write a program to print the given pattern
rows=3
1
1 c
1 e 3 */

import java.util.Scanner;

class Pattern8 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the number of rows: ");

        int rows = scanner.nextInt();

        int num = 1;
        char ch = 'a';

        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= i; j++) {
                if (j == i) {
                    if (i % 2 == 0) {
                        System.out.print(ch + " ");
                        ch += 2;  
                    } else {
                        System.out.print(num + " ");
                        num += 2; 
                    }
                } else {
                    System.out.print(j + " ");
                }
            }
            System.out.println();
        }
    }
}

