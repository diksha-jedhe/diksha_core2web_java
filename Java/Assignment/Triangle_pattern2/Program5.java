/* 5. Write a program to print the given pattern
rows =3

D
E F
G H I */

import java.util.Scanner;

class Pattern5 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();

        char ch = 'E';
        int count = 1;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j <= i; j++) {

                System.out.print(ch + " ");
                ch++;
                count++;
            }
            System.out.println();
        }

    }
}

