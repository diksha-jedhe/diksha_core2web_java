/*Q1 Write a program to print the following pattern.
Take a input from user using scanner.
$# $# $#
$# $# $#
$# $# $#*/

import java.util.Scanner;

class Pttern1{

	public static void main(String[] args){

		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter the number of the rows :");
		int numRows = scanner.nextInt();

		//System.out.println("Enter the number of the cols :");
		//int numCols = scanner.nextInt();

		for(int i = 0; i<= numRows; i++){
			for(int j = 0; j <= 3; j++){

				if(j % 2 == 0){
					System.out.print("$");
				}else{
					System.out.print("#");
				}if(j != 2){
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}
}

