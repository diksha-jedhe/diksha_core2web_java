/*Q8 Write a program to print the following pattern in java take a input from user
Number of rows = 4
d c b a
d c b a
d c b a
d c b a*/


import java.util.Scanner;

class Pattern7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the number of rows:");
        int numRows = scanner.nextInt();

        for (int i = 0; i < numRows; i++) {
           
            for (char j = 'd'; j >= 'a'; j--) {
                System.out.print(j + " ");
            }
            System.out.println(); 
        }


    }
}

