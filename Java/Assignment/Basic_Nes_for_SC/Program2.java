/*Q2 Write a program to print the following pattern.take a inccput from user.
1 2 3
1 2 3
1 2 3.*/

import java.util.Scanner;

class Pattern2 {

    	public static void main(String[] args) {
        	Scanner scanner = new Scanner(System.in);

        	System.out.println("Enter the number of rows:");
        	int numRows = scanner.nextInt();

        	for (int i = 0; i < numRows; i++) {
            
            		for (int j = 1; j <= 3; j++) {
                		System.out.print(j + " ");
            		}
            		System.out.println();
        	}
	}
}
