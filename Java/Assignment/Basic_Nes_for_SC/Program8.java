/*Q9 Write a program to print the following pattern in java take a input from the user
Number of rows = 3
C1 C2 C3
C4 C5 C6
C7 C8 C9*/


import java.util.Scanner;

class Pattern7{

	public static void main(String[] args){

		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter the number of rows :");
		int numRows = scanner.nextInt();

		int count = 1;

		for(int i = 0;i < numRows; i++){
			for(int j = 0;j < 3; j++){
				System.out.print("C" + count +" ");
				count++;
			}
			System.out.println();
		}
	}
}

