/*Q5 Write a program to print the following pattern
Take a input from user
A C E
G I K
M O Q*/


import java.util.Scanner;

class Pattern4 {

    	public static void main(String[] args) {
        	Scanner scanner = new Scanner(System.in);

        	System.out.println("Enter the number of rows:");
        	int numRows = scanner.nextInt();

        	int startAscii = 65;
        	int step = 2;

       
        	for (int i = 0; i < numRows; i++) {
            
            		int currentAscii = startAscii + (i * step);
            		for (int j = 0; j < 3; j++) {

                		System.out.print((char)currentAscii + " ");
                		currentAscii += 2;
            			}
            			System.out.println(); 
        	}
    	}
}
