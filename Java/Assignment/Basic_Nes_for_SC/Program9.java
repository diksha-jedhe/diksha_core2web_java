/*Q9 Write a program to print the following pattern in java take a input from the user
Number of rows = 4
1 2 3 4
4 5 6 7
7 8 9 10
10 11 12 13*/

import java.util.Scanner;

class Pattern8{
	public static void main(String[]rgs ){
		Scanner scanner = new Scanner(System.in);

		System.out.println("The enter the number of rows :");
		int numRows = scanner.nextInt();

		int start = 1;
		
		for(int i = 0; i < numRows; i++){
			for(int j = 0; j < 4; j++){
				System.out.print(start + " ");
				start++;
			}

			start--;
			start = start + 3;
			System.out.println();
		}
	}
}

		
