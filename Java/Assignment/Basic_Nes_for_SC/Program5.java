/*Q6 Write a program to print the following pattern
Take an input from the user
1A 1A 1A
1A 1A 1A
1A 1A 1A*/

import java.util.Scanner;

class Pattern5{

	public static void main(String[] args){

		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter the number for thr rows :");
		int numRows = scanner.nextInt();

		for(int i = 0; i <= numRows; i++){
			for(int j = 0; j < 3; j++){
				System.out.print("1A ");
			}
			System.out.println();
		}
	}
}
