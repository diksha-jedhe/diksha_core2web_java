/*Q7 Write a program to print the following pattern in java take a input from user
Number of rows = 3
1A 2B 3C
1A 2B 3C
1A 2B 3C*/

import java.util.Scanner;

class Pattern6{

	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter the number of rows :");
		int numRows = scanner.nextInt();

		for(int i = 0;i < numRows; i++){
			for(int j = 1;j <= 3;j++){
				System.out.print(j + "" + (char)('A' + i)+ " ");
			}
			System.out.println();
		}
	}
}

