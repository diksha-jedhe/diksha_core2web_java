/*Q4 Write a program to print the following pattern
Take a input from user.
1 1 1
2 2 2
3 3 3*/



import java.util.Scanner;

class Pattern3 {
    	public static void main(String[] args) {
        	Scanner scanner = new Scanner(System.in);

        	System.out.println("Enter the number of rows:");
        	int numRows = scanner.nextInt();

        	for (int i = 1; i <= numRows; i++) {
       
            		for (int j = 0; j < 3; j++) {
                		System.out.print(i + " ");
            		}
            		System.out.println();
        	}
    	}
}







