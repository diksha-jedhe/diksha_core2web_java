/* Q2. Write a program to find out the sum of all prime numbers in an array and
also print the count of prime numbers in an array.
Example:

5 7 9 12 17 19 21 22

Output:
Sum of all prime numbers is 48 and count of prime numbers in the given array is */


import java.util.Scanner;
class Array2{
       public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the size of the array:");
        int size = sc.nextInt();
        int[] arr = new int[size];
        System.out.println("Enter the elements of the array:");                                                for (int i = 0; i < size; i++) {
        arr[i] = sc.nextInt();
        }
        int oddSum = 0;
        int evenSum = 0;
        for (int j = 0; j < size; j++){
                if (arr[j] % 2 == 0) {
                        evenSum += arr[j];
                } else {
                        oddSum += arr[j];
                }
        }
        System.out.println("Odd Sum = " + oddSum);
        System.out.println("Even Sum = " + evenSum);
        }
}
