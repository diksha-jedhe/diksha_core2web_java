/* Q3. Write a program to check the count of the user given key in your array, if
the count is more than 2 times replace the element with its cube, print the
array.
Example:
arr1:
11 6 8 9 5 8 7 8 6

Input 1:
Enter key: 8
Output
Array will be like :
11 6 512 9 5 512 7 512 6 */

import java.util.Scanner;
class Array3{
       public static void main(String[] args) {
       Scanner sc = new Scanner(System.in);
         System.out.println("Enter the size of the array:");
         int size = sc.nextInt();
         int[] arr = new int[size];
         System.out.println("Enter the elements of the array:");                                                 for (int i = 0; i < size; i++) {
          arr[i] = sc.nextInt();
         }
          boolean isPalindrome = true;
          for (int i = 0; i < size / 2; i++) {
                  if (arr[i] != arr[size - 1 - i]) {
                          isPalindrome = false;
                          break;
                  }
          }
                         if (isPalindrome) {
                                 System.out.println("The given array is a palindrome array.");
                         } else {                                                                                                                                                                                                            System.out.println("The given array is not a palindrome array.");
                         }
                  }
}
