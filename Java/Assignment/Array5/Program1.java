import java.util.Scanner;
class Array1{


       public static void main(String[] args) {
	    
       Scanner scanner = new Scanner(System.in);
         System.out.println("Enter the size of the array:");
         int size = scanner.nextInt();
         int[] arr = new int[size];
         System.out.println("Enter the elements of the array:");
         for (int i = 0; i < size; i++) {
         arr[i] = scanner.nextInt();
        }
           boolean isAscending = true;
           for (int i = 1; i < size; i++) {
             if (arr[i] < arr[i - 1]) {
              isAscending = false;
                 break;
             }
           }
               if (isAscending) {
               System.out.println("The given array is in ascending Order.");
              } else {
               System.out.println("The given array is not in ascending Order.");
              }
       }
}
