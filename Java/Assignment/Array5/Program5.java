/* Q5. Take two different arrays where size of array may differ, you have to
create an array by combining both the arrays (you have to merge the arrays)
Example :

arr1
5 10 15 20 25 30 35

arr2
4 8 12 16 20

Array after merger :

5 10 15 20 25 30 35 4 8 12 16 20 */



import java.util.Scanner;
class Array5{
       public static void main(String[] args) {
       Scanner scanner = new Scanner(System.in);
       System.out.println("Enter the size of the array:");
       int size = scanner.nextInt();
        int[] arr = new int[size];
         System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++){
                arr[i] = scanner.nextInt();
        }
                System.out.print("Output: ");
                for (int i = 0; i < size; i++) {
                  int count = 0;
                  int num = arr[i];
                  while (num != 0) {
                          num /= 10;

                          count++;

                  }

                  System.out.print(count);

                  if (i < size - 1) {

                          System.out.print(", ");
                  }
                }
        }
}
