class Array9{
            public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter the Number:");
            long number = scanner.nextLong();
            int numDigits = String.valueOf(number).length();
             int[] digitsArray = new int[numDigits];
             for (int i = 0; i < numDigits; i++) {
             digitsArray[i] = (int) ((number % 10) + 1);
                     number /= 10;
             }
             System.out.print("Output: ");
               for (int i = 0; i < numDigits; i++) {
                       System.out.print(digitsArray[i]);
                       if (i < numDigits - 1) {
                               System.out.print(", ");
                       }
               }
            }
}
