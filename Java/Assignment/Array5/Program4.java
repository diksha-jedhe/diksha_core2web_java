/* Q4. You have to take two different 1d arrays of the same size and print the
common elements from the arrays.
arr1
45 67 97 87 90 80

arr2
15 97 67 80 90 10

Output:
Common elements in the given arrays are: 67, 97, 90, 80 */


import java.util.Scanner;
class Array4{
            public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter the size of the array:");
            int size = scanner.nextInt();
            int[] arr = new int[size];
            System.out.println("Enter the elements of the array:");
            for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
            }
                    int duplicateIndex = -1;
                    for (int i = 0; i < size - 1; i++) {
                      for (int j = i + 1; j < size; j++) {
                          if (arr[i] == arr[j]) {
                               duplicateIndex = i;
                               break;
                            }

                   }
                       if (duplicateIndex != -1) {
                           break;
                  }
        }
        if (duplicateIndex != -1) {
                System.out.println("First duplicate element present at index " + duplicateIndex);
         } else {
                System.out.println("No duplicate elements found in the array: ");
        }
	    }

}
