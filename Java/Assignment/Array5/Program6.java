/*
Q6. Write a program to check whether the array contains element multiple of
user given int value, if yes then print it’s index.
Example :
5 10 16 20 25 30 35 4 8 12 16 20
Input 1:
Enter key : 5
Output:
An element multiple of 5 found at index : 0
An element multiple of 5 found at index : 1
An element multiple of 5 found at index : 3
An element multiple of 5 found at index : 4
An element multiple of 5 found at index : 5
An element multiple of 5 found at index : 6
An element multiple of 5 found at index : 11*/

import java.util.Scanner;
class Array6{
            public static void main(String[] args) {
           Scanner scanner = new Scanner(System.in);
           System.out.println("Enter the size of the array:");
          int size = scanner.nextInt();
          int[] arr = new int[size];
          System.out.println("Enter the elements of the array:");
           for (int i = 0; i < size; i++) {
                   arr[i] = scanner.nextInt();
           }

           int primeIndex = -1;
           for (int i = 0; i < size; i++) {
                   if (arr[i] <= 1) {
                           continue;
                   }
                   boolean isPrime = true;
                   for (int j = 2; j <= Math.sqrt(arr[i]); j++) {
                           if (arr[i] % j == 0) {
                                   isPrime = false;
                                   break;
                           }
                   }
                   if (isPrime) {
                           primeIndex = i;
                           break;
                   }
           }
                           if (primeIndex != -1) {
                                  System.out.println("First prime number found at index " + primeIndex);                           } else {
                                  System.out.println("No prime number found in the array.");
                           }
            }
}
