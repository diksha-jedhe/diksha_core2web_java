import java.util.Scanner;
class Array8{
       public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the size:");
        int size = scanner.nextInt();
         int[] arr = new int[size];
         System.out.println("Enter the elements of the array:");
               for (int i = 0; i < size; i++) {
                       arr[i] = scanner.nextInt();
               }
               int min = Integer.MAX_VALUE;
               int secondMin = Integer.MAX_VALUE;
               for (int i = 0; i < size; i++) {
                       if (arr[i] < min) {
                               secondMin = min;
                               min = arr[i];
                       } else if (arr[i] < secondMin && arr[i] != min) {
                               secondMin = arr[i];
                       }
               }
               if (secondMin == Integer.MAX_VALUE) {
                       System.out.println("There is no second minimum element in the array.");
               } else {
                       System.out.println("The second minimum element in the array is: " + secondMin);
               }
       }
}
