/* Q6 Write a program to print the factorial of the number.
Rows = 3
c
3 2
c b a  */

import java.util.Scanner;

class FactorialAndPattern {
    // Function to calculate factorial of a number
    static int factorial(int n) {
        if (n == 0 || n == 1)
            return 1;
        else
            return n * factorial(n - 1);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number to calculate its factorial: ");
        int num = scanner.nextInt();

        // Calculate factorial of the number
        int fact = factorial(num);
        System.out.println("Factorial of " + num + " is: " + fact);

        // Print the pattern
        System.out.println("Pattern:");
        char c = 'c';
        for (int i = 1; i <= 3; i++) {
            char currentChar = c;
            for (int j = 1; j <= i; j++) {
                System.out.print(currentChar + " ");
                currentChar--;
            }
            System.out.println();
            c++;
        }
    }
}
