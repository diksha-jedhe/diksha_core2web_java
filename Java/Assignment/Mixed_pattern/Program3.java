/* Q3 Write a program to print the following pattern
Number of rows = 3
C B A
1 2 3
C B A */

import java.util.Scanner;

class Pattern3 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();

        char ch = 'C';
        int num = 1;

        for (int i = 0; i < rows; i++) {
            if (i % 2 == 0) {
                for (char c = 'C'; c >= 'A'; c--) {
                    System.out.print(c + " ");
                }
            } else {
                for (int j = 0; j < rows; j++) {
                    System.out.print(num + " ");
                    num++;
                }
            }
            System.out.println();
        }
    }
}

