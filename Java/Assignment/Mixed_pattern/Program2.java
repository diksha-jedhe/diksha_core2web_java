/* Q2 Write a program to print the following pattern
Number of rows = 3
C3 C2 C1
C4 C3 C2
C5 C4 C3 */


import java.util.Scanner;
class Pattern2 {

    public static void main(String[] args) {

        int rows = 3;
        int startValue = 3;

        for (int i = 0; i < rows; i++) {
            int currentValue = startValue;
            for (int j = 0; j < rows; j++) {
                System.out.print("C" + currentValue + " ");
                currentValue--;
            }
            startValue++;
            System.out.println();
        }
    }
}
