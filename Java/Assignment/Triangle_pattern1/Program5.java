/*5. WAP in notebook & Dry run first then type
Take number of rows from user :
Row = 3
1
2 4
3 6 9 */

import java.util.Scanner;

class Pattern5 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();
        
        int num = 1;
        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(num * j + " ");
            }
            num++;
            System.out.println();
        }
    }
}
