/* 9. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4
D C B A
D C B
D C
D */


import java.util.Scanner;

class Pattern10 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();

        char startChar = 'D';

        for (int i = 1; i <= rows; i++) {
            for (char j = startChar; j >= 'A'; j--) {
                System.out.print(j + " ");
            }
            startChar--;
            System.out.println();
        }
    }
}

