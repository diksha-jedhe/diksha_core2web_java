/*
6. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
3 3 3
2 2
1

*/

import java.util.Scanner;

class Pattern7 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();
     
        for (int i = rows; i >= 1; i--) {
            for (int j = 1; j <= i; j++) {
                System.out.print(i + " ");
            }
            System.out.println();
        }
    }
}

