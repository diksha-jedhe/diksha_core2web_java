/* 10. WAP in notebook & Dry run first then type
Take number of rows from user :
rows = 4

65 B 67 D
B 67 D
67 D
D  */

import java.util.Scanner;

class Pattern11 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();

        int start = 65; 

        for (int i = 1; i <= rows; i++) {
            for (int j = i; j <= rows; j++) {
                if (j % 2 == 0) {
                    System.out.print((char) (start + j) + " ");
		  
                } else {
                    System.out.print(start + " ");
                }
            }
            start+=2;
            System.out.println();
        }
    }
}

