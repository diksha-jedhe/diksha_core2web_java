/*Take the number of rows from the user.
 3
 3 6
 3 6 9 */


import java.util.Scanner;

class Pattern4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

	System.out.println("Enter the number of rows :");
	int rows = scanner.nextInt();

	for(int i = 1; i <= rows; i++){
		for(int j = 1;j <= i;j++){
			System.out.print((j * 3)+ " ");
		}
		System.out.println();
	}
    }
}
       
