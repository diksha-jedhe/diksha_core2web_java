/*WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
1
1 2
1 2 3  */

import java.util.Scanner;

class Pattern2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the number of rows:");
        int rows = scanner.nextInt();
        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
    }
}
