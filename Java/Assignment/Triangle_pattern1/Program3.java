/*3. WAP in notebook & Dry run first then type
Take number of rows from user in java :
Rows = 3
A
B C
C D E */

import java.util.Scanner;

class Pattern3 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();

        char startChar = 'A';

        for (int i = 0; i < rows; i++) {

            char printChar = startChar;

            for (int j = 0; j <= i; j++) {

                System.out.print(printChar + " ");
                printChar++;
            }
            System.out.println();
            startChar++;
        }
    }
}
