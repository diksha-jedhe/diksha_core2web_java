/* 2.WAP to print the sum of elements divisible by 3 in the array, where you have to take the
size and elements from the user.
Example:
Enter size : 8
9 13 5 13 6 22 36 10 */

import java.util.Scanner;
class Sum{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size :");
		int size = sc.nextInt();

		int[] array = new int[size];

		System.out.println("Enter " + size + "elements :");

		for(int i = 0;i < size;i++){
			array[i] = sc.nextInt();
		}
		int sum = 0;
		for(int i = 0;i < size;i++){
			if(array[i] % 3 == 0){
				sum = sum + array[i];

			}
		}
			System.out.println("The sum is :" + sum );
	}
}
		


