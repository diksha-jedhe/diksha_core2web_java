/* 4.WAP to search a specific element in an array and return its index. Ask the user to
provide the number to search, also take size and elements input from the user.
Example:
Input:
Enter the size
5
Enter elements:
12
144
13
156
8
Enter the number to search in array:
8

Output:
8 found at Index 4 */


import java.util.Scanner;
class SearchEle{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the size :");
		int size = sc.nextInt();

		int[] array = new int[size];

		System.out.println("Enter the Elemenets :");
		for(int i = 0; i < size; i++){
			array[i] = sc.nextInt();
		}
		 System.out.println("Enter the number to search in array:");
        	 int searchNumber = sc.nextInt();

        	boolean isFound = false;
        	for (int i = 0; i < array.length; i++) {
            		if (array[i] == searchNumber) {
                		System.out.println(searchNumber + " found at Index " + i);
                		isFound = true;
                		break;
            		}
        	}

       		if (!isFound) {
            		System.out.println(searchNumber + " not found in the array.");
        	}
    	}
}


