/* 6.Write a program to print the products of odd indexed elements in an array. Where you
have to take size input and elements input from the user.
Note:
Example:
Input:
Enter the size
6
Enter elements:
1
2
3
4
5
6
Output:
product of odd indexed elements : 48  */


import java.util.Scanner;
class Product{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the size :");
		int size = sc.nextInt();

		int[] array = new int[size];

		System.out.println("Enter Elements :");
		for(int i = 0;i < size ; i++){
			array[i] = sc.nextInt();
		}
		int prod = 1;
		for(int i = 1;i < array.length;i++){
			prod = prod * array[i];
		}
		System.out.println("Product of odd indexed elements :" + prod);
	}
}

