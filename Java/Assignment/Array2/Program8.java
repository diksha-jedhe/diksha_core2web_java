/* 8.WAP to print the elements in an array which are greater than 5 but less than 9, where
you have to take the size and elements from the user.
Example:
Input:
Enter the size
5
Enter elements:
4
8
6
9
41
Output:
8 is greater than 5 but less than 9
6 is greater than 5 but less than 9 */

import java.util.Scanner;
class Comparison{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the size :");
		int size = sc.nextInt();

		int[] array = new int[size];

		System.out.println("Enter elements :");
		for(int i = 0;i < size; i++){
			array[i] = sc.nextInt();
		}
		for (int num : array) {
            		if (num > 5 && num < 9) {
                		System.out.println(num + " is greater than 5 but less than 9");
            		}
		}
	}
}
