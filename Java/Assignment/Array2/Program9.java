/*  9. WAP to print the minimum element in the array, where you have to take the size and
elements from the user.
Example:
Input:
Enter the size
5
Enter elements:
5
6
9
-9
17
Output:

Minimum number in the array is : -9  */

import java.util.Scanner;
class MinEle{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the size :");
		int size = sc.nextInt();

		int[] array = new int[size];

		System.out.println("Enter elements:");
        	for (int i = 0; i < size; i++) {
            		array[i] = sc.nextInt(); 
       		}
		int min = array[0];
		for(int i = 0;i < size; i++){
			if(array[i] < min){
				min = array[i];
			}
		}
		System.out.println("Minimum number in the array is: " + min);
	}
}
