/* 1.WAP to count the even numbers in an array where you have to take the size and
elements from the user. And also print the even numbers too
Example:
Enter size =8
1 12 55 65 44 22 36 10
Output : even numbers 12 44 22 36 10
Count of even elements is : 5 */

import java.util.Scanner;
class EvenNum{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the size");
		int size = sc.nextInt();

		int array[] = new int[size];

		System.out.println("Enter " + size + " elements :");

		for(int i = 0; i < size; i++){
			array[i] = sc.nextInt();
		}
		int countEven = 0;
		System.out.println("Even Number :");
		for(int i = 0; i < size; i++){
			if(array[i] % 2 == 0){
				countEven++;
				System.out.println(array[i] + " ");
			}
	
		}
		
	}
}
		
