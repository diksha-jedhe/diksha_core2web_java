/*
Rows = 3
    1
  2 1 2
3 2 1 2 3
Rows = 4
      1
    2 1 2
  3 2 1 2 3
4 3 2 1 2 3 4
*/

import java.util.*;

class Pattern10 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Rows = ");
		int rows = sc.nextInt();

		for(int i=1; i<=rows; i++){

			for(int j=rows; j>=1; j--){

				if(j<=i)
					System.out.print(j + " ");
				else
					System.out.print("  ");
			}
			for(int j=2; j<=i; j++)
				System.out.print(j + " ");
			System.out.println();
		}
	}
}

