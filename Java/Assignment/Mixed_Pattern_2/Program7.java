/*
Rows = 3
    1
  3 2 1
5 4 3 2 1
Rows = 4
      1
    3 2 1
  5 4 3 2 1
7 6 5 4 3 2 1
*/

import java.util.*;

class Pattern7 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Rows = ");
		int rows = sc.nextInt();

		int num = 1;
		for(int i=0; i<rows; i++){

			for(int j=rows-1; j>=0; j--){
				if(j<=i)
					System.out.print(num-- + " ");
				else
					System.out.print("  ");
			}
			for(int j=i; j>0; j--)
				System.out.print(j + " ");
			System.out.println();
			num += i+3;
		}

	}
}

