/*
Rows = 3
    3
  2 3 4
1 2 3 4 5
Rows = 4
      4
    3 4 5
  2 3 4 5 6
1 2 3 4 5 6 7
*/

import java.util.*;

class Pattern8 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Rows = ");
		int rows = sc.nextInt();

		int num = 0;
		for(int i=rows; i>=1; i--){

			for(int j=1; j<=rows+num; j++){

				if(j>=i)
					System.out.print(j + " ");
				else
					System.out.print("  ");
			}
			System.out.println();
			num++;
		}
	}
}

