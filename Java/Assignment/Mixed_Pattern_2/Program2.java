/*
Rows = 3
1 2 3
  3 4
    4

Rows = 4
1 2 3 4
  4 5 6
    6 7
      7
*/

import java.util.*;

class Pattern2 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Rows = ");
		int rows = sc.nextInt();

		int num = 1;
		for(int i=1; i<=rows; i++){
			
			for(int j=1; j<=rows; j++){

				if(j>=i)
					System.out.print(num++ + " ");
				else
					System.out.print("  ");
			}
			num--;
			System.out.println();
		}
	}
}

