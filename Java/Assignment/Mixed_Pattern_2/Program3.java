/*
Rows = 3
      1
    4 7
10 13 16
Rows = 4
         1
      5  9
   13 17 21
25 29 33 37
*/

import java.util.*;

class Pattern3 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Rows = ");
		int rows = sc.nextInt();

		int num = 1;
		for(int i=1; i<=rows; i++){

			for(int j=rows; j>=1; j--){

				if(j<=i){
					System.out.print(num + " ");
					num += rows;
				}
				else
					System.out.print("   ");
			}
			System.out.println();
		}
	}
}

