/* Write the program and print all the consonants in the array */

class Array6{

	public static void main(String[] args){
		int arr[] = {'a','b','c','y','x','o','p'};
		for(int i = 0;i < arr.length;i++){
			if(arr[i] != 'a' && arr[i] != 'i' && arr[i] != 'o' && arr[i] != 'u' && arr[i] != 'e'){
				System.out.print((char)arr[i]+ " ");
				System.out.println();
			}
			//System.out.println();
		}
	}
}

