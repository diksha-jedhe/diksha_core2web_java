/* Q1. Write a program to check whether the given array is descending or not.
Input 1:
Enter the size of the array:
4
Enter the elements of the array:
15
9
5
1
Output :
Given array is in descending order. */

import java.util.Scanner;
class DescendingArr{

	public static void main(String[]args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the size of the Array :");
		int size = sc.nextInt();

		System.out.println("Enter the arrays elements :");
		int[] arr =  new int[size];
		for(int i = 0;i < size; i++){
			arr[i] = sc.nextInt();
		}
		int index = 1;
		while(index < size && arr[index] <= arr[index-1]){
			index++;
		}
		if(index == size){
			System.out.println("Given Array is in descending order.");
		}else{
			System.out.println("Given Array is not in descending order.");
		}
	}
}

