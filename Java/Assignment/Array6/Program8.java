/* Q8. Write a program to reverse the char array and print the alternate
elements of the array before and after reverse.

Size = 10
A B C D E F G H I J

After reverse :

J I H G F E D C B A

Output :
Before Reverse:
A C E G I

After Reverse:

J H F D B */

import java.util.Scanner;
class Array8{
            public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter the size of the array:");
             int size = scanner.nextInt();
             char[] charArray = new char[size];
             System.out.println("Enter the elements of the array:");
             for (int i = 0; i < size; i++) {
                     charArray[i] = scanner.next().charAt(0);
             }
              System.out.println("Before Reverse:");
                      for (int i = 0; i < size; i += 2) {
                          System.out.print(charArray[i] + " ");
                      }
                          System.out.println();
                         for (int i = 0, j = size - 1; i < j; i++, j--) {
                                 char temp = charArray[i];
                                 charArray[i] = charArray[j];
                                 charArray[j] = temp;
                         }
                         System.out.println("After Reverse:");
                           for (int i = 0; i < size; i += 2) {
                                 System.out.print(charArray[i] + " ");
                           }
            }
}
