/* Q6. Write a program to check whether the array contains element multiple of
user given int value, if yes then print it’s index.
Example :
5 10 16 20 25 30 35 4 8 12 16 20
Input 1:
Enter key : 5
Output:
An element multiple of 5 found at index : 0
An element multiple of 5 found at index : 1
An element multiple of 5 found at index : 3
An element multiple of 5 found at index : 4
An element multiple of 5 found at index : 5
An element multiple of 5 found at index : 6
An element multiple of 5 found at index : 11
Input 2:
Enter key: 99
Output : Element Not Found. */
