/* Q7. Write a program to, where you have to take integer input from user and if
the integer is in the range of ASCII value of A to Z, while saving you have to
type cast the int to char. Print the array.
Enter size :
7
Enter array elements :

55
67
65
97
36
13
68

Array will be like :
arr1
55 C B 97 36 13 D */


import java.util.Scanner;

class ASCIIRangeCon {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter size:");
        int size = scanner.nextInt();

        System.out.println("Enter array elements:");
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        for (int i = 0; i < size; i++) {
            if (arr[i] >= 'A' && arr[i] <= 'Z') {
                System.out.print((char) arr[i] + " ");
            } else {
                System.out.println(arr[i] + " ");
            }
        }
    }
}


