/* Q10. Write a program to find the third largest element in an array.
Example :
56 15 8 26 7 50 54

Output:
Third largest element is: 50 */

import java.util.Scanner;
class Array10{
            public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter the size of the array:");
             int size = scanner.nextInt();
             int[] arr = new int[size];
              System.out.println("Enter the elements of the array:");
              for (int i = 0; i < size; i++) {
                arr[i] = scanner.nextInt();
              }
               int firstMax = Integer.MIN_VALUE;
               int secondMax = Integer.MIN_VALUE;
               int thirdMax = Integer.MIN_VALUE;
               for (int i = 0; i < size; i++) {
               if (arr[i] > firstMax) {
                       thirdMax = secondMax;
                               secondMax = firstMax;
                               firstMax = arr[i];
                       } else if (arr[i] > secondMax && arr[i] != firstMax) {
                               thirdMax = secondMax;
                               secondMax = arr[i];
                       } else if (arr[i] > thirdMax && arr[i] != firstMax && arr[i] != secondMax) {
                               thirdMax = arr[i];
                       }
               }
               if (thirdMax != Integer.MIN_VALUE) {
                        System.out.println("Third largest element is: " + thirdMax);
                 } else {
                       System.out.println("There is no third largest element in the array.");

               }
            }
}
