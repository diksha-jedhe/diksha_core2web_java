/* Q3. Write a program to check the count of the user given key in your array, if
the count is more than 2 times replace the element with its cube, print the
array.
Example:
arr1:
11 6 8 9 5 8 7 8 6

Input 1:
Enter key: 8
Output
Array will be like :
11 6 512 9 5 512 7 512 6 */


import java.util.Scanner;

class KeyCount {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

      
        System.out.println("Enter the size of the array:");
        int size = sc.nextInt();

        System.out.println("Enter the elements of the array:");
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = sc.nextInt();
        }

        System.out.println("Enter the key:");
        int key = sc.nextInt();

        int count = 0;
        for (int i = 0; i < size; i++) {
            if (arr[i] == key) {
                count++;
            }
        }

        if (count > 2) {
            for (int i = 0; i < size; i++) {
                if (arr[i] == key) {
                    arr[i] = key * key * key;
                }
            }
        }
	else{
		System.out.println("Key Not found in Array :");
	}

        System.out.println("Array will be like:");
        for (int i = 0; i < size; i++) {
            System.out.println(arr[i] + " ");
        }
    }
}

