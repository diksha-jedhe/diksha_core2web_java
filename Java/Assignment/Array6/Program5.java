/* Q5. Take two different arrays where size of array may differ, you have to
create an array by combining both the arrays (you have to merge the arrays)
Example :
Q5. Take two different arrays where size of array may differ, you have to
create an array by combining both the arrays (you have to merge the arrays)
Example :

arr1
5 10 15 20 25 30 35

arr2
4 8 12 16 20

Array after merger :

5 10 15 20 25 30 35 4 8 12 16 20
arr1
5 10 15 20 25 30 35

arr2
4 8 12 16 20

Array after merger :

5 10 15 20 25 30 35 4 8 12 16 20 */

import java.util.Scanner;

class MergeArrays {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the size of the first array:");
        int size1 = sc.nextInt();

        System.out.println("Enter the elements of the first array:");
        int[] arr1 = new int[size1];
        for (int i = 0; i < size1; i++) {
            arr1[i] = sc.nextInt();
        }

        System.out.println("Enter the size of the second array:");
        int size2 = sc.nextInt();

        System.out.println("Enter the elements of the second array:");
        int[] arr2 = new int[size2];
        for (int i = 0; i < size2; i++) {
            arr2[i] = sc.nextInt();
        }

        int[] mergedArray = new int[size1 + size2];

        for (int i = 0; i < size1; i++) {
            mergedArray[i] = arr1[i];
        }

        for (int i = 0; i < size2; i++) {
            mergedArray[size1 + i] = arr2[i];
        }

        System.out.println("Array after merger:");

        for (int i = 0; i < mergedArray.length; i++) {

            System.out.print(mergedArray[i]);

            if (i < mergedArray.length - 1) {
                System.out.println(" ");
            }
        }
    }
}











