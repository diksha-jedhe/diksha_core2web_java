/* Q2. Write a program to find out the sum of all prime numbers in an array and
also print the count of prime numbers in an array.
Example:

5 7 9 12 17 19 21 22

Output:
Sum of all prime numbers is 48 and count of prime numbers in the given array is 4 */

import java.util.Scanner;

class SumAndCountOfPrimes {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the size of the array:");
        int size = scanner.nextInt();

        System.out.println("Enter the elements of the array:");
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        int sumOfPrimes = 0;
        int countPrime = 0;
        for (int i = 0; i < size; i++) {
            int num = arr[i];
            int divisor = 0;
            if (num > 1) {
                int limit = num * num; 
                for (int j = 2; j <= limit; j++) {
                    if (num % j == 0) {
                        divisor++;
                        break;
                    }
                }
                if (divisor == 0) {
                    sumOfPrimes += num;
                    countPrime++;
                }
            }
        }

        System.out.println("Sum of all prime numbers is " + sumOfPrimes + " and count of prime numbers in the given array is " + countPrime);
    }
}

