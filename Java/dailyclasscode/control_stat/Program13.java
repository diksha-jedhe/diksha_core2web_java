
class SwitchDemo{

	public static void main(String[] agrs){

		int num = 5;
		System.out.println("Before Switch");

		switch(num){
			case 1:
				System.out.println("one");
				break;
			case 2:
				System.out.println("Tww");
				break;
			case 3:
				System.out.println("Three");
				break;
			default:
				System.out.println("In default statement");
		}
		System.out.println("After Switch");
	}
}
