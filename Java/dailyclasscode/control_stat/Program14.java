
class SwitchDemo{

	public static void main(String[] agrs){

		int num = 1;
		System.out.println("Before Switch");

		switch(num){
			case 1:
				System.out.println("one");
			case 2:
				System.out.println("Two");
				break;
			case 3:
				System.out.println("Three");
				break;
			default:
				System.out.println("In default statement");
		}
		System.out.println("After Switch");
	}
}
