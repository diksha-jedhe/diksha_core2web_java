
import java.io.*;
class InputDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Your Name :");
		String name = br.readLine();

		br.close();

		System.out.println("Enter Society Name :");
		String SocName = br.readLine();

		System.out.println("Enter Your Wing :");
		char wing =(char) br.read();
		br.skip(1);

		System.out.println("Enter Your flat No :");
		String flatNo = br.readLine();

		System.out.println("Name :" + name);
		System.out.println("SocName :" + SocName);
		System.out.println("Wing :" + wing);
		System.out.println("flat No :" + flatNo);

	}
}
