/*Q4.
Write a program to check the day number(1-7) and print the corresponding day of
week.
Input : 5
Output : Friday
Input : 7
Output : Sunday*/

/*class Week{
	public static void main(String[] args){

	int DayNo = 2;
	switch (DayNo) {
            case 1:
                System.out.println("Monday");
                break;
            case 2:
                System.out.println("Tuesday");
                break;
            case 3:
                System.out.println("Wednesday");
                break;
            case 4:
                System.out.println("Thursday");
                break;
            case 5:
                System.out.println("Friday");
                break;
	    default:
                System.out.println("Wrong Input");
		}
	}
}*/


class Week{
	public static void main(String[] args){

		int dayNumber = 4;
	
		if (dayNumber == 1) {
            		System.out.println("Monday");
        	} else if (dayNumber == 2) {
            		System.out.println("Tuesday");
        	} else if (dayNumber == 3) {
            		System.out.println("Wednesday");
        	} else if (dayNumber == 4) {
            		System.out.println("Thursday");
        	} else if (dayNumber == 5) {
            		System.out.println("Friday");
		}else{
            		System.out.println("Invalid Input");
		}
	}
}



