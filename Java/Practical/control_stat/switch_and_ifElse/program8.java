/*Q8.
Write a program in which takes two numbers if both the numbers are positive
multiply them and provide to switch case to verify whether the number is even or
odd, if the user enters any negative number program should terminate by saying
“Sorry negative numbers not allowed”*/

class OddEven{

	public static void main(String[] args){

		int num1 = 4;
		int num2 = 6;

		if (num1 < 0 || num2 < 0) {
            		System.out.println("Sorry negative numbers not allowed");
            		return;
        	}

        	int result = num1 * num2;
        		System.out.println("Result of multiplication: " + result);

        // Using switch case to verify if the result is even or odd
        	switch (result % 2) {
            		case 0:
                		System.out.println("Result is even");
                		break;
            	default:
                		System.out.println("Result is odd");
        }
    }
}

/*class OddEven{

	public static void main(String[] args){
		int num1 = -1;
		int num2 = 5;

		if (num1 < 0 || num2 < 0) {
            		System.out.println("Sorry negative numbers not allowed");
            		return; 
        	}

        		int result = num1 * num2;
       				 System.out.println("Result of multiplication: " + result);

        	// Using if-else to verify if the result is even or odd
        	if (result % 2 == 0) {
           		 System.out.println("Result is even");
        	} else {
            		System.out.println("Result is odd");
        	}
    	}
}*/
















