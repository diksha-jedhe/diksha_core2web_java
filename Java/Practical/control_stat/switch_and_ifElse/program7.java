/*Q7.
Write a program for the below example.
If you are a trip planner and you are planning a trip according to the budget of
client*/

/*class Trip{

	public static void main(String[] args){

	int budget = 10000;

	switch (budget) {
            	case 15000:
                	System.out.println("Destination: Jammu and Kashmir");
                	break;
            	case 10000:
                	System.out.println("Destination: Manali");
                	break;
            	case 6000:
                	System.out.println("Destination: Amritsar");
                	break;
            	case 2000:
                	System.out.println("Destination: Mahabaleshwar");
                	break;
            	default:
                	System.out.println("Other budgets: Try next time");
        }
	}
	
}*/

class Trip{

	public static void main(String[] args){

		int budget = 1000;

 		if (budget == 15000) {
            		System.out.println("Destination: Jammu and Kashmir");
        	} else if (budget == 10000) {
            		System.out.println("Destination: Manali");
        	} else if (budget == 6000) {
            		System.out.println("Destination: Amritsar");
        	} else if (budget == 2000) {
           		System.out.println("Destination: Mahabaleshwar");
        	} else {
            		System.out.println("Other budgets: Try next time");
        }
    }
}











