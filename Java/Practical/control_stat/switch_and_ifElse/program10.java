/*Q10.
Write a real time example with at least 5 cases.*/

/*class HotelMenuCard{

	public static void main(String[] args){

		int choice = 3;
		switch (choice) {
            		case 1:
                		System.out.println("Appetizers Menu:Salad");
                		break;
            		case 2:
                		System.out.println("Main Courses Menu:Pizza");
                		break;		
            		case 3:
                		System.out.println("Desserts Menu:Ice-cream");
                		break;
            		case 4:
                		System.out.println("Beverages Menu:Juice");
                		break;
            		case 5:
                		System.out.println("Specials of the Day:Spl Food");
                		break;
            		default:
                		System.out.println("Invalid choice! Please enter a number between 1 and 5.");
        }
    }
}*/

class HotelMenuCard{

	public static void main(String[] args){

		int choice = 5;
		if (choice == 1) {
            		System.out.println("Appetizers Menu");
        	} else if (choice == 2) {
            		System.out.println("Main Courses Menu");
        	} else if (choice == 3) {
            		System.out.println("Desserts Menu");
        	} else if (choice == 4) {
            		System.out.println("Beverages Menu");
        	} else if (choice == 5) {
            		System.out.println("Specials of the Day");
        	} else {
           		 System.out.println("Invalid choice! Please enter a number between 1 and 5.");
        }
    }
}



