/* WAP to check the float value is divisible bye 6 or not. */

class FloatNo{

	public static void main(String[] args){

		float num = 120.6f;

		if(num % 6 == 0){
			System.out.println(num + " is divisible by 6");
		}else{
			System.out.println(num + " is not divisible by 6");
		}
	}
}


