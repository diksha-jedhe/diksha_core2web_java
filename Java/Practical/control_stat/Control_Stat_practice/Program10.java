/*10.Dry run the given code in your notebook where you have to check if there is any error
if yes mention so also if there is no error write the output.*/

class WhileDemo{

	public static void main(String[] args){
		int num1 = 10;
		int num2 = 1;
		while(--num1>num2++){
			System.out.println(num1 + num2);
		}
		System.out.println("Value of num1: "+num1);
		System.out.println("Value of num2: "+num2);
	}
}
