/*5. Write a program to check whether the given number is divisible by 7 or not.
Input : 105
Output: Divisible by 7
Input: -31
Output: Not divisible by 7.*/


class DivideNo{

	public static void main(String[] args){

		int num = 101;

		if(num%7 == 0){
			System.out.println("The number is divisible by seven");
		}else{
			System.out.println("The number is not divisible by seven");
		}
	}
}

