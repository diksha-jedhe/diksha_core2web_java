/*4. Write a program to check whether the given Character is in UPPERCASE (Capital) or
in lowercase. (take hardcoded values)
Input: ch = ‘a’;
Output: a is a lowercase character
Input: ch = ‘A’;
Output: A is an UPPERCASE character.*/


class charactor{

    public static void main(String[] args) {

        char ch = 'A';

        if (ch >= 'A' && ch <= 'Z') {
            System.out.println(ch + " is an UPPERCASE character.");
        } else if (ch >= 'a' && ch <= 'z') {
            System.out.println(ch + " is a lowercase character.");
        } else {
            System.out.println(ch + " is neither uppercase nor lowercase.");
        }
    }
}

