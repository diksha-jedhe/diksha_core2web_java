/*9. Write a program to check the day number(1-7) and print the corresponding day of week.
Input : 1
Output: Monday
Input : 7
Output: Sunday
Input : 8
Output: day number must be between 1 to 7.*/

class Days {
    public static void main(String[] args) {

        int dayNumber = 4;

        if (dayNumber == 1) {
            System.out.println("Monday");

        } else if (dayNumber == 2) {
            System.out.println("Tuesday");

        } else if (dayNumber == 3) {
            System.out.println("Wednesday");

        } else if (dayNumber == 4) {
            System.out.println("Thursday");

        } else if (dayNumber == 5) {
            System.out.println("Friday");

        } else if (dayNumber == 6) {
            System.out.println("Saturday");

        } else if (dayNumber == 7) {
            System.out.println("Sunday");

        } else {
            System.out.println("Day number must be between 1 to 7!!!!");
        }
    }
}

