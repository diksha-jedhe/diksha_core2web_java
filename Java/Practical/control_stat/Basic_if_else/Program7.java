/*7. Write a program to find the maximum between two distinct numbers. (take hardcoded
values)*/

class MaxNo  {
    public static void main(String[] args) {
        int num1 = 5;
        int num2 = 9;

        if (num1 > num2) {
            System.out.println("The maximum number is: " + num1);
        } else {
            System.out.println("The maximum number is: " + num2);
        }
    }
}
