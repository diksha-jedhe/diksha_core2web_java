/*10.
Write a program where you have to use both for and while loop.*/
class forWhile{
	public static void main(String[] args){
	int number = 10;
        
        int sumOfEvens = 0;
        int productOfOdds = 1;
        
        // Using for loop to calculate sum of even numbers and product of odd numbers
        for (int i = 1; i <= number; i++) {
            if (i % 2 == 0) {
                sumOfEvens += i;
            } else {
                productOfOdds *= i;
            }
        }
        
        System.out.println("Sum of even numbers: " + sumOfEvens);
        System.out.println("Product of odd numbers: " + productOfOdds);
        
        // Using while loop to calculate sum of even numbers and product of odd numbers
        System.out.println("\nCalculating using while loop:");
        int x = 1;
        while (x <= number) {
            if (x % 2 == 0) {
                sumOfEvens += x;
            } else {
                productOfOdds *= x;
            }
            x++;
        }
        
        System.out.println("Sum of even numbers: " + sumOfEvens);
        System.out.println("Product of odd numbers: " + productOfOdds);
    }
}
