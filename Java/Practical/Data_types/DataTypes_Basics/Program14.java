/*Write a program to print the characters using given ASCII values :
67,79,82,69,50,87,69,66*/

class Demo{

	public static void main(String[] args){
		int [] asciiVal = {67,79,82,69,50,87,69,66};
		for(int i = 1; i < 8; i++){
			System.out.println((char)(asciiVal[i]%128)+" ");
		}
	}
}
