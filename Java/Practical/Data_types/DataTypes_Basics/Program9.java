/*9. Write a program to print the temperature of the Air Conditioner in
degrees and also print the standard room temperature using appropriate
data types.*/

class Temprature{

	public static void main(String[] args){
		float acConditioner_temp = 24.6f;
		float room_Temperature = 20.5f;

		System.out.println(acConditioner_temp);
		System.out.println(room_Temperature);
	}
}
