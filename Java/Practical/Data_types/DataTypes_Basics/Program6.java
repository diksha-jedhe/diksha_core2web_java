/*6. A person is storing date, month and year in variables (Write a code to
print the date, month, year, and also print the total seconds in a day,
month and year.)*/

class Calender{

	public static void main(String[] args){

		int year = 2005;
		int date = 28;
		String month = "June";
		int SecondInDay = 24*60*60;
		double SecondMonth = 30.44*30*60*60;
		double SecondInYear = 365*24*60*60;

		System.out.println(year);
		System.out.println(date);
		System.out.println(month);
		System.out.println(SecondInDay);
		System.out.println(SecondMonth);
		System.out.println(SecondInYear);
	}
}
