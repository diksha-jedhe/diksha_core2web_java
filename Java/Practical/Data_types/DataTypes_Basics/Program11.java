/*11. Write a program to print the value of gravity and the print the letter
used to represent the acceleration due to gravity.*/

class Gravity{

	public static void main(String[] args){
		float gravity = 9.8f;
		char gravityLetter = '9';

		System.out.println(gravity);
		System.out.println(gravityLetter);
	}
}

